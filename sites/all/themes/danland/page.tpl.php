<?php
/**
 * @file
 * Default theme implementation to display Danland page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template normally located in the
 * modules/system folder.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. themes/danland.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 * - $hide_site_name: TRUE if the site name has been toggled off on the theme
 *   settings page. If hidden, the "element-invisible" class is added to make
 *   the site name visually hidden, but still accessible.
 * - $hide_site_slogan: TRUE if the site slogan has been toggled off on the
 *   theme settings page. If hidden, the "element-invisible" class is added to
 *   make the site slogan visually hidden, but still accessible.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['search_box']: Items for the search box.
 * - $page['superfish_menu']: Items for the superfish menu.
 * - $page['slideshow']: Items for the slideshow.
 * - $page['preface']: Items for the preface top region / set only displayed on frontpage.
 * - $page['highlighted']: Items for the highlighted region.
 * - $page['preface_first']: Items for the preface first region.
 * - $page['preface_middle']: Items for the preface middle region.
 * - $page['preface_last']: Items for the preface last region.
 * - $page['sidebar_first']: Items for the left sidebar / sidebar first.
 * - $page['sidebar_second']: Items for the right sidebar / sidebar second.
 * - $page['content_top']: Items for the content top region.
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['content']: The main content of the current page / content middle.
 * - $page['content_bottom']: Items for the content bottom region.
 * - $page['bottom_first']: Items for the bottom first region.
 * - $page['bottom_middle']: Items for the bottom middle region.
 * - $page['bottom_last']: Items for the bottom last region.
 * - $page['bottom_1']: Items for the bottom 1 region.
 * - $page['bottom_2']: Items for the bottom 2 region.
 * - $page['bottom_3']: Items for the bottom 3 region.
 * - $page['bottom_4']: Items for the bottom 4 region.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 */
?>
<div <?php print danland_page_class($page['sidebar_first'], $page['sidebar_second']); ?>>
	<div id="header">
		<div id="header-wrapper">
			<?php if ($logo): ?> 
				<div id="logo-wrapper">
					<div class="logo">
						<a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><img src="<?php print $logo ?>" alt="<?php print t('Home') ?>" /></a>
					</div>
				</div><!-- end logo wrapper -->
			<?php endif; ?>
			<?php if ($site_name || $site_slogan) : ?>
				<div id="branding-wrapper">
					<?php if ($site_name) : ?>
						<?php if ($is_front) : ?>
							<h1 class="site-name"><a href="<?php print $base_path ?>" title="<?php print $site_name ?>"><?php print $site_name ?></a></h1>
						<?php endif; ?>
						<?php if (!$is_front) : ?>
							<h2 class="site-name"><a href="<?php print $base_path ?>" title="<?php print $site_name ?>"><?php print $site_name ?></a></h2>
						<?php endif; ?>
					<?php endif; ?>
					<?php if ($site_slogan) : ?>
						<div class='site-slogan'>
							<?php print $site_slogan; ?>
						</div> <!-- end site-slogan -->
					<?php endif; ?>
				</div><!-- end branding wrapper -->
			<?php endif; ?>
			<?php if ($page['search_box']): ?>
				<div id="search-box">
					<?php print render ($page['search_box']); ?>
				</div> <!--  end search-box -->
			<?php endif; ?>
			<?php if ($feed_icons): ?>
				<div class="feed-wrapper">
					<?php print $feed_icons; ?>
				</div> <!-- end RSS -->
			<?php endif; ?>
			<?php if (!$is_admin): ?>
				<div id="authorize">
					<ul>
						<?php global $user; 
							if ($user->uid != 0) { 
								print '<li class="first">' .t('Logged in as '). '<a href="' .url('user/'.$user->uid). '">' .$user->name. '</a></li>'; 
								print '<li><a href="' .url('user/logout'). '">' .t('Logout'). '</a></li>'; } 
							else { 
								print '<li class="first"><a href="' .url('user'). '">' .t('Login'). '</a></li>'; 
								print '<li><a href="' .url('user/register'). '">' .t('Register'). '</a></li>'; } 
						?>
					</ul>
				</div> <!-- end login / register -->
			<?php endif; ?>
		</div> <!-- end header-wrapper -->
	</div> <!-- header -->
	
	<div style="clear:both"></div>
	
	<div id="menu">
		<div id="rounded-menu-left"></div>
			<?php if ($main_menu || $page['superfish_menu']): ?>
				<div id="<?php print $main_menu ? 'nav' : 'superfish' ; ?>">
					<?php 
						if ($main_menu) {
							print theme('links__system_main_menu', array('links' => $main_menu));
						}
						elseif (!empty($page['superfish_menu'])) {
							print render ($page['superfish_menu']);
						} 
					?>
				</div> <!-- end primary / superfish -->
			<?php endif; ?>
		<div id="rounded-menu-right"></div>
	</div> <!-- end menu -->
	
	<div style="clear:both"></div>

	<?php if($is_front): ?>
		<div id="slideshow-wrapper">
			<div class="slideshow-inner">
				<?php if ($page['preface']): ?>
					<div id="slideshow-preface">
						<div id="preface">
							<?php print render ($page['preface']); ?>
						</div> <!-- end preface -->
					</div>
				<?php endif; ?>
				<?php if ($page['highlighted']) : ?>
					<div id="slideshow-bottom">
						<div id="mission">
							<?php print render ($page['highlighted']); ?>
						</div>
					</div> <!-- end mission / highlighted -->
				<?php endif; ?>
				<?php if ($page['slideshow']) : ?>
					<div class="slideshow">
						<?php print render ($page['slideshow']); ?>
					</div> <!-- end slideshow -->
				<?php endif; ?>
				<?php if (!$page['slideshow']) : ?>
					<div class="slideshow">
						<!-- GP start header rotation -->
						<?php
							$mask = '/\.(?:jp[e]?g|gif|png)$/i';
							$files = file_scan_directory(drupal_get_path('theme', 'danland') . '/images/slideshows', $mask);
							foreach ($files as $filename => $data) {
						?>
							<img src="<?php print $filename; ?>" width="950" height="355" alt="slideshow"/>
						<?php } ?>
						<!-- end header rotation -->
					</div> <!-- end slideshow -->
				<?php endif; ?>
			</div>
		</div>
	<?php endif; ?>

	<?php if($page['preface_first'] || $page['preface_middle'] || $page['preface_last']) : ?>
		<div style="clear:both"></div> <!-- Do not touch -->
		<div id="preface-wrapper" class="in<?php print (bool) $page['preface_first'] + (bool) $page['preface_middle'] + (bool) $page['preface_last']; ?>">
			<?php if($page['preface_first']) : ?>
				<div class="column A">
					<?php print render ($page['preface_first']); ?>
				</div>
			<?php endif; ?>
			<?php if($page['preface_middle']) : ?>
				<div class="column B">
					<?php print render ($page['preface_middle']); ?>
				</div>
			<?php endif; ?>
			<?php if($page['preface_last']) : ?>
				<div class="column C">
					<?php print render ($page['preface_last']); ?>
				</div>
			<?php endif; ?>
			<div style="clear:both"></div>
		</div> <!-- end preface first etc. -->
	<?php endif; ?>

	<div style="clear:both"></div>

	<div id="wrapper">
		<?php if ($page['sidebar_first']): ?>
			<div id="sidebar-left" class="column sidebar">
				<div class="section">
					<?php print render($page['sidebar_first']); ?>
				</div>
			</div> <!-- end sidebar-first -->
		<?php endif; ?>
		<div id="content">
			<a id="main-content"></a>
			<?php if ($page['content_top']) : ?>
				<div class="content-top">
					<?php print render ($page['content_top']); ?>
				</div>
			<?php endif; ?>
			<?php if (!$is_front) print $breadcrumb; ?>
			<?php if ($show_messages) { print $messages; }; ?>
      		<?php print render($title_prefix); ?>
      		<?php if ($title): ?>
        		<h1 class="title" id="page-title">
					<?php print $title; ?>
        		</h1>
			<?php endif; ?>
      		<?php print render($title_suffix); ?>
      		<?php if ($tabs): ?>
        		<div class="tabs">
          			<?php print render($tabs); ?>
        		</div>
      		<?php endif; ?>
      		<?php print render($page['help']); ?>
      		<?php if ($action_links): ?>
        		<ul class="action-links">
          			<?php print render($action_links); ?>
        		</ul>
      		<?php endif; ?>
			<?php if ($page['content']) : ?>
				<div class="content-middle">
					<?php print render ($page['content']); ?>
				</div>
			<?php endif; ?>
			<?php if ($page['content_bottom']) : ?>
				<div class="content-bottom">
					<?php print render ($page['content_bottom']); ?>
				</div>
			<?php endif; ?>
		</div> <!-- end content -->
		<?php if ($page['sidebar_second']): ?>
			<div id="sidebar-right" class="column sidebar">
				<div class="section">
					<?php print render($page['sidebar_second']); ?>
				</div>
			</div> <!-- end sidebar-second -->
		<?php endif; ?>
		<div style="clear:both"></div>
	</div> <!-- end wrapper -->

	<?php if($page['bottom_first'] || $page['bottom_middle'] || $page['bottom_last']) : ?>
		<div style="clear:both"></div> <!-- Do not touch -->
		<div id="bottom-teaser" class="in<?php print (bool) $page['bottom_first'] + (bool) $page['bottom_middle'] + (bool) $page['bottom_last']; ?>">
			<?php if($page['bottom_first']) : ?>
				<div class="column A">
					<?php print render ($page['bottom_first']); ?>
				</div>
			<?php endif; ?>
			<?php if($page['bottom_middle']) : ?>
				<div class="column B">
					<?php print render ($page['bottom_middle']); ?>
				</div>
			<?php endif; ?>
			<?php if($page['bottom_last']) : ?>
				<div class="column C">
					<?php print render ($page['bottom_last']); ?>
				</div>
			<?php endif; ?>
			<div style="clear:both"></div>
		</div> <!-- end bottom first etc. -->
	<?php endif; ?>

	<?php if($page['bottom_1'] || $page['bottom_2'] || $page['bottom_3'] || $page['bottom_4']) : ?>
		<div style="clear:both"></div> <!-- Do not touch -->
		<div id="bottom-wrapper" class="in<?php print (bool) $page['bottom_1'] + (bool) $page['bottom_2'] + (bool) $page['bottom_3'] + (bool) $page['bottom_4']; ?>">
			<?php if($page['bottom_1']) : ?>
				<div class="column A">
					<?php print render ($page['bottom_1']); ?>
				</div>
			<?php endif; ?>
			<?php if($page['bottom_2']) : ?>
				<div class="column B">
					<?php print render ($page['bottom_2']); ?>
				</div>
			<?php endif; ?>
			<?php if($page['bottom_3']) : ?>
				<div class="column C">
					<?php print render ($page['bottom_3']); ?>
				</div>
			<?php endif; ?>
			<?php if($page['bottom_4']) : ?>
				<div class="column D">
					<?php print render ($page['bottom_4']); ?>
				</div>
			<?php endif; ?>
			<div style="clear:both"></div>
		</div> <!-- end bottom -->
	<?php endif; ?>

	<div style="clear:both"></div>

	<div id="footer-wrapper">
		<?php if($page['footer']): ?>
			<div id="footer">
				<?php print render ($page['footer']); ?>
			</div> <!-- end footer -->
		<?php endif; ?>
		
		<?php if($secondary_menu) : ?>
			<div id="subnav-wrapper">
				<?php print theme('links__system_secondary_menu', array('links' => $secondary_menu, 'attributes' => array('id' => 'subnav', 'class' => array('links', 'clearfix')))); ?>
			</div> <!-- end secondary menu -->
		<?php endif; ?>
	</div> <!-- end footer wrapper -->
	
	<div style="clear:both"></div>
	
	<div id="notice"><p>Theme by <a href="http://www.danetsoft.com">Danetsoft</a> and <a href="http://www.danpros.com">Danang Probo Sayekti</a> inspired by <a href="http://www.maksimer.no">Maksimer</a></p></div>
</div>
