<?php

$style = theme_get_setting('style');

switch ($style) {
	case 1:
		drupal_add_css(drupal_get_path('theme', 'current') . '/css/style1.css', array('group' => CSS_THEME, 'weight' => 100, 'type' => 'file'));
		break;
	case 2:
		drupal_add_css(drupal_get_path('theme', 'current') . '/css/style2.css', array('group' => CSS_THEME, 'weight' => 100, 'type' => 'file'));
		break;
	case 3:
		drupal_add_css(drupal_get_path('theme', 'current') . '/css/style3.css', array('group' => CSS_THEME, 'weight' => 100, 'type' => 'file'));
		break;
	case 4:
		drupal_add_css(drupal_get_path('theme', 'current') . '/css/style4.css', array('group' => CSS_THEME, 'weight' => 100, 'type' => 'file'));
		break;
	case 5:
		drupal_add_css(drupal_get_path('theme', 'current') . '/css/style5.css', array('group' => CSS_THEME, 'weight' => 100, 'type' => 'file'));
		break;
	default:
		drupal_add_css(drupal_get_path('theme', 'current') . '/css/style1.css', array('group' => CSS_THEME, 'weight' => 100, 'type' => 'file'));

}

drupal_add_css(drupal_get_path('theme', 'current') . '/css/responsive.css', array('group' => CSS_THEME, 'weight' => 101, 'type' => 'file'));
drupal_add_css(drupal_get_path('theme', 'current') . '/css/custom.css', array('group' => CSS_THEME, 'weight' => 101, 'type' => 'file'));

// Allow a site-specific user defined CSS file (useful for multisite installations):
// If a CSS file "local-[SITE].css" is residing in the "css" directory (beside "local.css"),
// it will be loaded after "local.css". SITE is the site's host name, without leading "www".
// For example, for the site http://www.mydomain.tld/ the file must be called called "local-[mydomain.tld].css"
global $base_url;
$site = preg_replace("/^[^\/]+[\/]+/", '', $base_url);
$site = preg_replace("/[\/].+/", '', $site);
$site = preg_replace("/^www[^.]*[.]/", '', $site);
drupal_add_css(path_to_theme() . '/css/local-[' . $site . '].css', 'theme', 'all');

/* First and Last Classes on Teasers */

function current_preprocess_page(&$variables) {
  // Hide the main content if the quick tab block "Most Read" is present.
  if (isset($variables['page']['content']['quicktabs_most_read'])) {
    hide($variables['page']['content']['system_main']);
  }

  if(isset($variables['page']['content']['system_main']['nodes'])){
    $nodes = $variables['page']['content']['system_main']['nodes'];
    $i = 1;
    $len = count($nodes);
    foreach (array_keys($nodes) as $nid) {
      if ($i == 1) {
        $variables['page']['content']['system_main']['nodes'][$nid]['#node']->classes_array = array('first');
      }
      if ($i == $len - 1) {
        $variables['page']['content']['system_main']['nodes'][$nid]['#node']->classes_array = array('last');
      }
      $i++;
      /* So I don't get "Warning: Cannot use a scalar value as an array" */
      unset($nodes,$nid);
    }
  }

}

function current_preprocess_node(&$variables) {
  $node = $variables['node'];
  if (!empty($node->classes_array)) {
    $variables['classes_array'] = array_merge($variables['classes_array'], $node->classes_array);
  }

  if (!$variables['page']) {
    if (isset($variables['node']->view) &&
      ($variables['node']->view->name == 'most_read' || $variables['node']->view->name == 'most_read_headline')) {
      if ($variables['node']->view->current_display == 'block') {
        $variables['node_url'] .= '?most_read_source=year';
      }
      elseif ($variables['node']->view->current_display == 'block_1') {
        $variables['node_url'] .= '?most_read_source=week';
      }
      elseif ($variables['node']->view->current_display == 'block_2') {
        $variables['node_url'] .= '?most_read_source=month';
      }
      elseif ($variables['node']->view->current_display == 'block_3') {
        $variables['node_url'] .= '?most_read_source=all';
      }
    }
  }

}

/* Breadcrumbs */

function current_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];
  $output = "";
  if (!empty($breadcrumb)) {
   
    // Provide a navigational heading to give context for breadcrumb links to
    // screen-reader users. Make the heading invisible with .element-invisible.

    $output .= '<div class="breadcrumb"><div class="breadcrumb-inner">' . implode(' / ', $breadcrumb) . '</div></div>';
    return $output;
  }
}

/* Span Tag on Links */

function current_link($variables) {
  return '<a href="' . check_plain(url($variables['path'], $variables['options'])) . '"' . drupal_attributes($variables['options']['attributes']) . '><span>' . ($variables['options']['html'] ? $variables['text'] : check_plain($variables['text'])) . '</span></a>';
}

/* Some text in ye old Search Form */

function current_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'search_block_form') {

// Add extra attributes to the text box
    $form['search_block_form']['#attributes']['onblur'] = "if (this.value == '') {this.value = 'Search';}";
    $form['search_block_form']['#attributes']['onfocus'] = "if (this.value == 'Search') {this.value = '';}";
    // Prevent user from searching the default text
    $form['#attributes']['onsubmit'] = "if(this.search_block_form.value=='Search'){ alert('Please enter a search'); return false; }";
  }
} 

/* Add Page Body Class */

function current_preprocess_html(&$vars) {
  $path = drupal_get_path_alias($_GET['q']);
  $aliases = explode('/', $path);

  foreach($aliases as $alias) {
    $vars['classes_array'][] = drupal_clean_css_identifier($alias);
  } 
  
  /* Add Custom Archive Page Template - Mikal Dickerson, 01-15-2016 */
  if (module_exists('path')) {
    	$alias2 = drupal_get_path_alias(str_replace('/edit','',$_GET['q']));
    	if ($alias2 != $_GET['q']) {
      		$template_filename = 'html';
      		foreach (explode('/', $alias2) as $path_part) {
        		$template_filename = $template_filename . '__' . $path_part;
        		$vars['theme_hook_suggestions'][] = $template_filename;
      		}
    	}
  	}
	/* Custom Archive Page End Edits */
	
  $detect = mobile_detect_get_object();

  if ($detect->isMobile() && !$detect->isTablet()) {
    $vars['classes_array'][] = 'mobile';
  }
  elseif ($detect->isTablet()) {
    $vars['classes_array'][] = 'tablet';
  }
  if ($detect->isiOS()) {
    $vars['classes_array'][] = 'ios';
  }
  if ($detect->isAndroidOS()) {
    $vars['classes_array'][] = 'android';
  }
}

/**
 * Implements template_preprocess_views_view_field().
 */
function current_preprocess_views_view_field(&$vars) {
  $view = $vars['view'];

  if ($view->name === 'most_read_headline') {
    
    if ($vars['field']->field_alias === 'node_title') {
      $path = drupal_get_path_alias('node/' . $vars['row']->nid);
      $options = array();

      switch ($view->current_display) {
        case 'block':
        case 'attachment_1':
          $most_read_source = 'year';
          break;

        case 'block_1':
        case 'attachment_2':
          $most_read_source = 'week';
          break;

        case 'block_2':
          $most_read_source = 'month';
          break;

        case 'block_3':
          $most_read_source = 'all';
          break;
      }
      if (!empty($most_read_source)) {
        $options['query'] = array(
          'most_read_source' => $most_read_source
        );
      }

      $vars['output'] = l($vars['row']->node_title, $path, $options);
    }
  }
}

/**
 * Implements hook_html_head_alter().
 */
function current_html_head_alter(&$head_elements) {
  $head_elements['metatag_og:image_0']['#value'] = 'http:' . $head_elements['metatag_og:image_0']['#value'];
  $head_elements['metatag_og:image:url_0']['#value'] = 'http:' . $head_elements['metatag_og:image:url_0']['#value'];
  $head_elements['metatag_og:image:secure_url_0']['#value'] = 'http:' . $head_elements['metatag_og:image:secure_url_0']['#value'];

}