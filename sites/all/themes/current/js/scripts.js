(function ($) {

    $(document).ready(function(){

     if($('#block-system-main-menu').hasClass('contextual-links-region')) {
      $('#block-system-main-menu').removeClass('contextual-links-region');
    }
    $('#main-menu ul.menu li ul.menu').addClass('sub-menu');   
    $("#main-menu ul.menu li")
    .mouseenter(function() {
      $(this).find('.sub-menu').show();
    })
    .mouseleave(function() {
      $(this).find('.sub-menu').hide();
    });

    if ($.browser.msie) { } else {
    $('ul.menu').mobileMenu({
    combine: true,
    switchWidth: 760,
    prependTo: "#main-menu",
    nested: false,
    groupPageText: 'More',
    topOptionText: 'Select a page'
    });
    }
    
    equalHeight($("#preface .block-inner"));
	// 22-01-2015
	$('#node-151').parent().parent().parent().hide();
	$('h1#page-title').hide();
	
    }); 

  Drupal.behaviors.bonesSuperfish = {
  
    attach: function(context, settings) {
        
    $('#user-menu ul.menu', context).superfish({
      delay: 400,                         
      animation: {height:'show'},
      speed: 500,
      easing: 'easeOutBounce', 
      autoArrows: false,
      dropShadows: false /* Needed for IE */
    });
      
    }
    } 
        
  function equalHeight(group) {
  tallest = 0;
  group.each(function() {
    thisHeight = $(this).height();
    if(thisHeight > tallest) {
      tallest = thisHeight;
    }
  });
  group.height(tallest);
  }

  // We are overriding viewsScrollTop() from the Views module so that we can
  // scroll as desired on the Most Read page.
  Drupal.ajax.prototype.commands.viewsScrollTop = function (ajax, response, status) {
    // Scroll to the top of the view. This will allow users
    // to browse newly loaded content after e.g. clicking a pager
    // link.
    var offset = $(response.selector).offset();
    // We can't guarantee that the scrollable object should be
    // the body, as the view could be embedded in something
    // more complex such as a modal popup. Recurse up the DOM
    // and scroll the first element that has a non-zero top.
    var scrollTarget = response.selector;
    while ($(scrollTarget).scrollTop() == 0 && $(scrollTarget).parent()) {
      scrollTarget = $(scrollTarget).parent();
    }
    // Only scroll upward
    if (offset.top - 10 < $(scrollTarget).scrollTop()) {
      var scrollTop = offset.top - 10;

      // For the Most Read view we want to scroll a little bit more up so that
      // the page title is visible.
      if ($(response.selector).hasClass('view-most-read')) {
        scrollTop -= 200;
      }
      $(scrollTarget).animate({scrollTop: (scrollTop)}, 500);
    }
  };

})(jQuery);  