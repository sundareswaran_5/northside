(function ($) {

    $(document).ready(function(){

     if($('#block-system-main-menu').hasClass('contextual-links-region')) {
      $('#block-system-main-menu').removeClass('contextual-links-region');
    }
    $('#main-menu ul.menu li ul.menu').addClass('sub-menu');   
    $("#main-menu ul.menu li")
    .mouseenter(function() {
      $(this).find('.sub-menu').show();
    })
    .mouseleave(function() {
      $(this).find('.sub-menu').hide();
    });

    if ($.browser.msie) { } else {
    $('ul.menu').mobileMenu({
    combine: true,
    switchWidth: 760,
    prependTo: "#main-menu",
    nested: false,
    groupPageText: 'More',
    topOptionText: 'Select a page'
    });
    }
    
    equalHeight($("#preface .block-inner"));
	// 22-01-2015
	$('#node-151').parent().parent().parent().hide();
	$('h1#page-title').hide();
	
    }); 

  Drupal.behaviors.bonesSuperfish = {
  
    attach: function(context, settings) {
        
    $('#user-menu ul.menu', context).superfish({
      delay: 400,                         
      animation: {height:'show'},
      speed: 500,
      easing: 'easeOutBounce', 
      autoArrows: false,
      dropShadows: false /* Needed for IE */
    });
      
    }
    } 
        
  function equalHeight(group) {
  tallest = 0;
  group.each(function() {
    thisHeight = $(this).height();
    if(thisHeight > tallest) {
      tallest = thisHeight;
    }
  });
  group.height(tallest);
  }

})(jQuery);  