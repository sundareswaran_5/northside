<?php
$i = 0;
$infeed1 = block_get_blocks_by_region('infeed1');
$infeed2 = block_get_blocks_by_region('infeed2');
$infeed3 = block_get_blocks_by_region('infeed3');
$infeed4 = block_get_blocks_by_region('infeed4');
$infeed1a = block_get_blocks_by_region('infeed1a');
$infeed2a = block_get_blocks_by_region('infeed2a');
$infeed3a = block_get_blocks_by_region('infeed3a');
$infeed4a = block_get_blocks_by_region('infeed4a');
/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<?php foreach ($rows as $id => $row): ?>
  <div<?php if ($classes_array[$id]): ?> class="<?php print $classes_array[$id]; ?>"<?php endif; ?>>
    <?php 	$i++;
			print $row; 
			print '<hr style="margin-bottom: 20px;">';
	?>    
  </div>
  <?php if ($i == 2 && views_get_page_view()) { 
                  ?><div class="ad"><?php
  			print render($infeed1);
                 ?></div><hr style="margin-bottom: 20px;"><?php
		} elseif ( $i == 4 && views_get_page_view()) {
                ?><div class="ad"><?php
			print render($infeed2); 
                ?></div><hr style="margin-bottom: 20px;"><?php
		} elseif ( $i == 6 && views_get_page_view()) {
                ?><div class="ad"><?php
			print render($infeed3); 
                ?></div><hr style="margin-bottom: 20px;"><?php
		} elseif ( $i == 8 && views_get_page_view()) {
                ?><div class="ad"><?php
            print render($infeed4);
                ?></div><hr style="margin-bottom: 20px;"><?php
		}
  ?>
<?php endforeach; ?>
