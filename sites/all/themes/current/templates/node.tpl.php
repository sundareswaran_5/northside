<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>



  <?php if (isset($content['field_image'])): ?>

    <div class="field-image">

      <?php print render($content['field_image']); ?>

    </div>

  <?php endif; ?>



  <?php if (isset($content['field_video'])): ?>

    <div class="field-video">

      <?php print render($content['field_video']); ?>

    </div>

  <?php endif; ?>



  <div class="node-content-wrapper">



    <?php print render($title_prefix); ?>



      <?php  if (!$page): ?>

        <h2 class="title"<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>

      <?php endif; ?>



      <?php if ($page): ?>

       <h1 class="title"<?php print $title_attributes; ?>><?php print $title; ?></h1>

      <?php endif; ?>



    <?php print render($title_suffix); ?>



    <?php

        if($type == 'article') {

          if ($node->field_author) {



            $tid = $node->field_author['und'][0]['tid'];

            $tid_ld = taxonomy_term_load($tid);

            print 'By ' . strtoupper(l(t($tid_ld->name),'author-content/'.$tid)) . ',';



          }

        } else {

          print $name;

        }

     ?>



    <?php if (isset($content['links'])): ?>

        <?php print render($content['links']); ?>

    <?php endif; ?>



    <?php if ($display_submitted): ?>

      <div class="submitted">

            <span class="date"><?php print $date; ?></span>

      </div>

    <?php endif; ?>



    <div class="content"<?php print $content_attributes; ?>>

    <?php

      // We hide the comments and links now so that we can render them later.

    hide($content['field_image']);

    hide($content['field_video']);

      hide($content['comments']);

      hide($content['links']);

    hide($content['field_tags']);

    hide($content['field_section']);







if ( !empty($node) && $node->type == 'article') {



  // Setup the Mobile Detection variables

  $detect = mobile_detect_get_object();

  $is_mobile = $detect->isMobile();

  $is_tablet = $detect->isTablet();


/*
    if($is_mobile){

      $p1 = array(4,36,68,100);

      $p2 = array(8,40,72,104);

      $p3 = array(12,44,76,108);

      $p4 = array(16,48,80,112);

      $p5 = array(20,52,84,116);

      $p6 = array(24,56,88,120);

      $p7 = array(28,60,92,124);

      $p8 = array(32,64,96,128);

    } else {

      // Which paragraphs should we place ads after?

      $p1 = array(4,36,68,100);

      $p2 = array(8,40,72,104);

      $p3 = array(12,44,76,108);

      $p4 = array(16,48,80,112);

      $p5 = array(20,52,84,116);

      $p6 = array(24,56,88,120);

      $p7 = array(28,60,92,124);

      $p8 = array(32,64,96,128);

    }

*/

    // Set the instory block variables

    $instory1 = block_get_blocks_by_region('instory1');

    $instory2 = block_get_blocks_by_region('instory2');

    $instory3 = block_get_blocks_by_region('instory3');

    $instory4 = block_get_blocks_by_region('instory4');

    $instory1a = block_get_blocks_by_region('instory1a');

    $instory2a = block_get_blocks_by_region('instory2a');

    $instory3a = block_get_blocks_by_region('instory3a');

    $instory4a = block_get_blocks_by_region('instory4a');

    $story_ads = array(
      block_get_blocks_by_region('instory1'),
      block_get_blocks_by_region('instory1a'),
      block_get_blocks_by_region('instory2'),
      block_get_blocks_by_region('instory2a'),
      block_get_blocks_by_region('instory3'),
      block_get_blocks_by_region('instory3a'),
      block_get_blocks_by_region('instory4'),
      block_get_blocks_by_region('instory4a'),
    );

    // Break out the body
    $array = explode("</p>", render($content['body']));

    if($is_mobile){
      $next_ad_index = 0;
      $ad_class = 'left';
      for ($i = 5; $i < count($array); $i += 5) {
        $array[$i] = '<div class="ad">
        <div class="' . $ad_class . '">' . render($story_ads[$next_ad_index]) . '</div>
        </div>' . $array[$i];

        $next_ad_index++;
        if ($next_ad_index >= count($story_ads)) {
          $next_ad_index = 0;
        }

        if ($ad_class === 'left') {
          $ad_class = 'right';
        }
        else {
          $ad_class = 'left';
        }
      }
    } else {
      $next_ad_index = 0;
      $ad_class = 'left';
      for ($i = 7; $i < count($array); $i += 7) {
        $array[$i] = '<div class="ad">
        <div class="' . $ad_class . '">' . render($story_ads[$next_ad_index]) .   '</div>
        </div>' . $array[$i];

        $next_ad_index++;
        if ($next_ad_index >= count($story_ads)) {
          $next_ad_index = 0;
        }

        if ($ad_class === 'left') {
          $ad_class = 'right';
        }
        else {
          $ad_class = 'left';
        }
      }    
    }



    $content['body'] = implode("</p>", $array);

    print render($content['body']);

    print render($content['add_to_cart']);

    print render( $content['webform'] );



    //print render( $content['flippy_pager'] );

    print render( $content['pager_for_content_type'] );



    //print render($content);

  } // end the node type check



  else {

    print render($content['body']);

    print render($content['add_to_cart']);

    print render( $content['webform'] );

    print render( $content['flippy_pager'] );



    // print render($content);

  }





      ?>

    </div>







    <?php if (isset($content['field_section'])): ?>

      <nav class="taxonomy"><div class="taxonomy-inner clearfix">

        <?php print render($content['field_section']); ?>

      </div></nav>

    <?php endif; ?>



  <?php if (isset($content['field_tags'])): ?>

      <nav class="taxonomy"><div class="taxonomy-inner clearfix">

        <?php print render($content['field_tags']); ?>

      </div></nav>

    <?php endif; ?>



 <?php

 if ( !empty($node) && $node->type == 'advpoll') {

     if (isset($content['advpoll_choice'])): ?>



 <div class="poll-choice">

  <?php print render($content['advpoll_choice']); ?>

 </div>

    <?php endif; ?>

<?php } ?>







  </div>





</article>



<?php print render($content['comments']); ?>

