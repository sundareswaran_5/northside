<?php
/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
$adBlock = array(
  1 => array(
    block_get_blocks_by_region('infeed1'),
  ),
  3 => array(
    block_get_blocks_by_region('infeed2'),
  ),
  5 => array(
    block_get_blocks_by_region('infeed3'),
  ),
  7 => array(
    block_get_blocks_by_region('infeed4'),
  ),
);
?>
<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>

<?php foreach ($rows as $id => $row): ?>
  <div<?php if ($classes_array[$id]) { print ' class="' . $classes_array[$id] .'"';  } ?>>
    <?php print $row; ?>
    <?php print '<hr style="margin-bottom: 20px;">';?>
  </div>
  <?php
    if (!empty($adBlock[$id])) {
      ?><div class="ad"><?php 
      foreach ($adBlock[$id] as $ad) { 
        print render($ad);
      }
     ?></div><?php
    }
  ?>
<?php endforeach; ?>