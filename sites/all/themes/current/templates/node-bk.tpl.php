<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>> 
  
  <?php if (isset($content['field_image'])): ?>
    <div class="field-image">
      <?php print render($content['field_image']); ?>
    </div>
  <?php endif; ?>
  
  <?php if (isset($content['field_video'])): ?>
    <div class="field-video">
      <?php print render($content['field_video']); ?>
    </div>
  <?php endif; ?>      
  
  <div class="node-content-wrapper">                    
  
    <?php print render($title_prefix); ?>
          
      <?php  if (!$page): ?>
        <h2 class="title"<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
      <?php endif; ?>
  
      <?php if ($page): ?>
       <h1 class="title"<?php print $title_attributes; ?>><?php print $title; ?></h1>  
      <?php endif; ?>
         
    <?php print render($title_suffix); ?>
    
    <?php if ($display_submitted): ?>
      <div class="submitted">
          <span class="date"><?php print $date.' by '; ?></span>
          <?php if($type == 'article') { ?>
          	<?php $tid = $node->field_author['und'][0]['tid'];
          		   $tid_ld = taxonomy_term_load($tid);
          	?>
          	<?php print l(t($tid_ld->name),'author-content/'.$tid); ?>
          <? } else {
          	print $name;
          } ?>
      </div>
    <?php endif; ?>      
  
    <div class="content"<?php print $content_attributes; ?>>
	  <?php
      // We hide the comments and links now so that we can render them later.
	  hide($content['field_image']);
	  hide($content['field_video']);
      hide($content['comments']);
      hide($content['links']);
	  hide($content['field_tags']);
	  hide($content['field_section']);



if ( !empty($node) && $node->type == 'article') {
                 
                // Setup the Mobile Detection variables
                $detect = mobile_detect_get_object();
                $is_mobile = $detect->isMobile();
                $is_tablet = $detect->isTablet();
      		
                if($is_mobile){
      		$p1 = 4;
      		$p2 = 12;
      		$p3 = 18;
                $p4 = 24;
                } else {
                // Which paragraphs should we place ads after?
      		$p1 = 4;
      		$p2 = 12;
      		$p3 = 18;
                $p4 = 24;
                }

      		// Set the instory block variables
		$instory1 = block_get_blocks_by_region('instory1');
		$instory2 = block_get_blocks_by_region('instory2');
		$instory3 = block_get_blocks_by_region('instory3');
                $instory4 = block_get_blocks_by_region('instory4');
		// Break out the body	
		$array = explode("</p>", render($content['body']));
		// Find the last key of the array to make sure we don't show ads if the content isn't long enough
		end($array);         
		$key = key($array);
		// Now we need to reset the pointer back to the 0 index
		reset($array);
		// First ad placed
		if ($p1 <= $key && !views_get_page_view()){
                $array[$p1] = render($instory1). $array[$p1]; 
		}
		// Second ad placed
		if ($p2 <= $key){
		$array[$p2] = render($instory2). $array[$p2];
		}
		// Third ad placed
		if ($p3 <= $key){
		$array[$p3] = render($instory3). $array[$p3];
		}
                if ($p4 <= $key){
		$array[$p4] = render($instory4). $array[$p4];
		}		
		


		$content['body'] = implode("</p>", $array);
       	print render($content['body']);	
	print render($content['add_to_cart']);
        print render( $content['webform'] );
        print render( $content['flippy_pager'] );

		//print render($content);
	} // end the node type check
	
	else {
       	print render($content['body']);
print render($content['add_to_cart']);
print render( $content['webform'] );
print render( $content['flippy_pager'] );

		// print render($content);
     }

// print render($content);

      ?>
    </div>
    
    <?php if (isset($content['links'])): ?>  
      <?php print render($content['links']); ?>
    <?php endif; ?>
    
    <?php if (isset($content['field_section'])): ?>  
      <nav class="taxonomy"><div class="taxonomy-inner clearfix">
        <?php print render($content['field_section']); ?>
      </div></nav>  
    <?php endif; ?>    
  
	<?php if (isset($content['field_tags'])): ?>  
      <nav class="taxonomy"><div class="taxonomy-inner clearfix">
        <?php print render($content['field_tags']); ?>
      </div></nav>  
    <?php endif; ?>
    
 <?php
 if ( !empty($node) && $node->type == 'advpoll') {
     if (isset($content['advpoll_choice'])): ?>  

 <div class="poll-choice"> 
  <?php print render($content['advpoll_choice']); ?>
 </div>
    <?php endif; ?>
<?php } ?>


 
  </div>
    

</article>

<?php print render($content['comments']); ?> 
