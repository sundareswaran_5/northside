<?php
/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
$i = 0;
?>
<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>

<?php foreach ($rows as $id => $row): ?>
  
  <?php if ($i === 1): ?>
    <div class="attachment">
  <?php endif; ?>

  <div <?php if ($classes_array[$id]) { print 'class="' . $classes_array[$id] .'"';  } ?>>
    <?php
      print $row;
      $i++;
    ?>
  </div>
<?php endforeach; ?>

<?php if (count($rows) > 1): ?>
</div>
<?php endif; ?>