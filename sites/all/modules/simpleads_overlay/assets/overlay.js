(function($) {

  Drupal.behaviors.simpleadsOverlay = {
    attach: function (context) {   

      // call leanModal when ajax on simpleads block ends
      $(".block-simpleads").one("ajaxStop", function() {
          if( $(this).find('#simpleads-overlay-wrapper').length) {
            var adImages = $("#simpleads-overlay-wrapper img");
            // if there are images, call modal after images are loaded
            if (adImages.length > 0) {
              adImages.one("load",function(){
                $('body').leanModal();
              });
            } else {
              $('body').leanModal();
            }
          }
      });

      // add click event to close button
      $('#simpleads-overlay-close').live('click', function(){  
        var nid = $('#simpleads-overlay-close').attr('class').replace('simplead-overlay-', '');  
        $.ajax({
          url:"/simpleads-overlay/close", 
          data: {aid: nid},
          success:function(result){
            $("#lean_overlay").fadeOut(300);
            $('#simpleads-overlay-wrapper').css({ 'display' : 'none' });
          }
        });
      }); 

      // add click event to any link in overlay ad
      $('#simpleads-overlay-wrapper a').live('click', function(e){  
        var nid = $('#simpleads-overlay-close').attr('class').replace('simplead-overlay-', '');  
        e.preventDefault();
        var linkTo = $(this).attr("href");
        $.ajax({
          url:"/simpleads-overlay/close", 
          data: {aid: nid},
          success:function(result){
            $("#lean_overlay").fadeOut(300);
            $('#simpleads-overlay-wrapper').css({ 'display' : 'none' });
            location.href = linkTo; 
          }
        });
      }); 

    }   
  }

  $.fn.extend({ 
      centerToWindow: function() {
        var obj           = $(this);
        var obj_width     = $(this).outerWidth(true);
        var obj_height    = $(this).outerHeight(true);
        var window_width  = window.innerWidth ? window.innerWidth : $(window).width();
        var window_height = window.innerHeight ? window.innerHeight : $(window).height();
        obj.css({
          "position" : "fixed",
          "top"      : ((window_height / 2) - (obj_height / 2))+"px",
          "left"     : ((window_width / 2) - (obj_width / 2))+"px"
        });
      },
      leanModal: function() {
          var overlay_opacity = 0.6;
          var overlay = $("<div id='lean_overlay'></div>");

          if ($('#simpleads-overlay-wrapper').length > 0) {
            $('body').append(overlay);
            $('#lean_overlay').css({ 'display' : 'block', opacity : 0 });
            $('#lean_overlay').fadeTo(300,overlay_opacity);

            $('#simpleads-overlay-wrapper').centerToWindow();
            $('#simpleads-overlay-wrapper').fadeTo(200,1);
          }
      }
  });

})(jQuery);