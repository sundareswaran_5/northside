<?php


/* 
*     hook_init()
*/
function simpleads_overlay_init() {
  drupal_add_css(drupal_get_path('module', 'simpleads_overlay') . "/assets/overlay.css");
  drupal_add_js(drupal_get_path('module', 'simpleads_overlay') . "/assets/overlay.js");
}

/**
 * Implements hook_menu().
 */
function simpleads_overlay_menu() {
  $items['admin/config/system/simpleads-overlay'] = array(
    'title' => 'SimpleAds Overlay',
    'description' => 'Configure the display of overlays',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('simpleads_overlay_config'),
    'access arguments' => array('administer onthisdate settings'),
  );
  
  $items['simpleads-overlay/close'] = array(
    'title' => 'Close Ad',
    'page callback' => 'simpleads_overlay_close',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );

  return $items;
}

function simpleads_overlay_config()  {
  $form = array();
  
  $form['appear'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Appear'),
    '#options' => array('1' => 'appear once' , '2' => 'appear at every connection'),
    '#default_value' => variable_get('appear', 1),
  );
  
  return system_settings_form($form);
}


/**
 * Implement hook_ctools_plugin_api().
 * This hook is needed to let ctools know about exportables.
 * If you create field groups by using hook_field_group_info, you
 * will need to include the ctools api hook as well.
 */
function simpleads_overlay_ctools_plugin_api($module, $api) {
  if ($module == 'field_group' && $api == 'field_group') {
    return array('version' => 1);
  }
}

/**
* Implementation of hook_field_group_info()
* 
*   - to move field instances on simpleads node and on ad_groups vocab forms into a fieldset
*/
function simpleads_overlay_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass;
  $field_group->api_version = 1;
  $field_group->identifier = 'node';
  $field_group->group_name = 'simpleads_overlay';  
  $field_group->entity_type = 'node';
  $field_group->bundle = 'simpleads';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'SimpleAds Overlay',
    'weight' => '10',
    'children' => array(
      0 => 'simpleads_overlay_autoclose',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsed',
      'instance_settings' => array(
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['node|simpleads_overlay'] = $field_group;
  
  $field_group = new stdClass;
  $field_group->api_version = 1;
  $field_group->identifier = 'taxonomy_term';
  $field_group->group_name = 'simpleads_overlay';  
  $field_group->entity_type = 'taxonomy_term';
  $field_group->bundle = 'ad_groups';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'SimpleAds Overlay',
    'weight' => '10',
    'children' => array(
      0 => 'simpleads_overlay_enabled',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsed',
      'instance_settings' => array(
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['taxonomy_term|simpleads_overlay'] = $field_group;
  
  return $field_groups;
}

/**
 * Alter SimpleAd output.
 *
 * @param array $data
 */
function simpleads_overlay_simpleads_alter(&$data){
  $remove = &drupal_static('simpleads_overlay_remove');
  $adblock = &drupal_static('simpleads_overlay_block');
  
  $ad_group = current(entity_load('taxonomy_term', array($data['node']->field_ad_category['und'][0]['tid'])));
  if (!$ad_group->simpleads_overlay_enabled['und'][0]['value']) {
    return;
  }
  
  $adblock = $ad_group->tid;

  global $user;
  if ($user->uid) {
    $uid = $user->uid;
  }
  else {
    $uid = ip_address();
  }
  
  // check if this user has already closed this ad
  $remove = FALSE;
  $x = db_query("SELECT user FROM {simpleads_overlay} WHERE user = :uid AND nid = :nid", array(":uid" => $uid, ":nid" => $data['node']->nid))->fetchField();  
  if ($x) {
    $remove = TRUE;
    $data[$data['type']] = '';
    return;
  }
 
  if ($data['type'] != 'graphic') {
    // add CLOSE button
    $data[$data['type']] .= '<div id="simpleads-overlay-close" class="simplead-overlay-' . $data['node']->nid . '">CLOSE</div>';
    // add wrapper
    $data[$data['type']] = '<div id="simpleads-overlay-wrapper">' . $data[$data['type']] . '</div>';
  } 
  else {
    $data['overlay'] = 1;
    $data['overlay_preimage'] = '<div id="simpleads-overlay-wrapper">';
    $data['overlay_postimage'] = '<div id="simpleads-overlay-close" class="simplead-overlay-' . $data['node']->nid . '">CLOSE</div></div>';
  }

  return;
} 

function simpleads_overlay_block_view_alter(&$data, $block) {
  $remove = &drupal_static('simpleads_overlay_remove');
  $adblock = &drupal_static('simpleads_overlay_block');
  
  // only alter our overlay ad block
  if ($block->delta != 'ad_groups_' . $adblock) {
    return;  
  }
  
  // always remove block Title for our overlay ads
  $data['subject'] = '';
  
  // and then if the ad is not supposed to be presented; remove the content as well
  if ($remove) { 
    $data['content'] = '';   
  }
  
  return;
} 

/**
* AJAX callback to log that this user has CLOSED this ad
* 
* 
*/
function simpleads_overlay_close($data) {
  global $user;
  
  if ($user->uid) {
    $uid = $user->uid;
  }
  else {
    $uid = ip_address();
  }
  
  $aid = $_GET['aid'];
  
  if ($aid && $uid) {
    db_insert('simpleads_overlay') 
      ->fields(array(
        'user' => $uid,
        'nid' => $aid,
      ))
      ->execute(); 
  }
}
