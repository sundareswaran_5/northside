<?php
// include_once('.' . base_path() . drupal_get_path('module', 'system') . '/system.admin.inc');
// Above line was intentionally commented out (see below).
define('DRUPAL_ROOT', getcwd());
include_once DRUPAL_ROOT . '/includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
$drupal_version = (int) VERSION;
$list_modules_function = '';
if ($drupal_version >= 7 && $drupal_version < 8) {
  $list_modules_function = 'system_rebuild_module_data';
}
else if ($drupal_version >= 6 && $drupal_version < 7) {
  $list_modules_function = 'module_rebuild_cache';
}
if (empty($list_modules_function)) {
  $output = t('Oops... Looks like you are not using either version 6 or version 7 of Drupal');
}
else if (!function_exists($list_modules_function)) {
  $output = t('Oops... Unable to find the function !function(). Try uncommenting the top line of this code.', array('!function' => $list_modules_function));
}
else {
  $output = "<dl>\n";
  $list_modules = $list_modules_function();
  echo "Total Modules:".count($list_modules)."<br/>";
  $packagedModules = array();
  $enabledModules = array();
  foreach ($list_modules as $module) {
      $subModule["name"] = check_plain($module->info["name"]);
      $subModule["version"] = check_plain($module->info["version"]);
      $subModule["description"] = check_plain($module->info["description"]);
      $subModule["status"] = check_plain($module->status);
      if ($module->status == 1) {
          $enabledModules[] = $subModule;
      }
      $packagedModules[$module->info["package"]][] = $subModule;
  }
  echo "Total Enabled Modules:".count($enabledModules)."<br/>";
  $i=1;
  foreach ($packagedModules as $package => $moduleList) {
      $output .= "<dt><b>".$package."</b></dt>\n";
      $count = $i;
      foreach ($moduleList as $module) {
        if ($module["status"] == 1) {
            $output .= "<dt>". $module["name"] . " - ".$module["version"]."</dt>\n";
            $output .= "<dd>" . $module["description"] . "</dd>\n<br/>";
        }
      }
      $i++;
  }
  $output .= "</dl>\n";
  echo "Total Enabled Packages: ".$count;
}
print $output;
?>